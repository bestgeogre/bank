<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Send units';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to send units to other user:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'send-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
        
        <div class="form-group">
            <label class="col-lg-1 control-label">Sender</label>
            <span class="col-lg-3 control-label" style="text-align: left;"><strong><?= $model->getSender()->username?></strong> (Current balance: <?= number_format($model->getSender()->balance, 2)?>)</span>
        </div>
        <?= $form->field($model, 'sum')->textInput() ?>
        <?= $form->field($model, 'receiver_id')->dropDownList($users); ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Send', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
        
    

    <?php ActiveForm::end(); ?>
</div>
