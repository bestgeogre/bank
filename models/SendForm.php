<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * SendForm is the model behind the send money form.
 *
 * @property User $sender This property is read-only.
 * @property User $receiver
 * @property number $sum
 *
 */
class SendForm extends Model
{
    public $sender_id;
    public $receiver_id;
    public $sum;

    private $_sender = false;
    private $_receiver = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['sum', 'validateSum'],
            [['sender_id', 'receiver_id'], 'required'],
            [['sender_id', 'receiver_id'], 'integer'],
            ['sender_id', 'compare', 'compareAttribute' => 'receiver_id', 'operator' => '!='],
            [['sender_id', 'receiver_id'], 'exist', 'targetClass' => User::class, 'targetAttribute' => ['sender_id' => 'id', 'receiver_id' => 'id']],
            ['sum', 'number', 'min' => 0],
            
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'receiver_id' => 'Receiver',
            'sum' => 'Sum',            
        ];
    }

    /**
     * Transfer sum from user to user
     * @return bool whether the transfer was successful
     */
    public function send()
    {
        if($this->validate())
        {
            $sender = $this->getSender();
            $receiver = $this->getReceiver();
            if($sender && $receiver)
            {
                User::getDb()->transaction(function($db) use ($sender, $receiver) {
                   $sender->balance -= $this->sum;
                   $receiver->balance += $this->sum;
                   $sender->save();
                   $receiver->save();
                   
                   $transaction = new Transactions(['sender_id' => $sender->id,
                                                    'recipient_id' => $receiver->id,
                                                    'sum' => $this->sum]);
                   $transaction->save();

                });
            }
            return true;
        }
        return false;
    }
    
    /**
     * Finds sender by [[id]]
     *
     * @return User|null
     */
    public function getSender()
    {
        if(!$this->_sender)
        {
            $this->_sender = $this->getUser($this->sender_id);
        }
        return $this->_sender;
    }
    
    
    /**
     * Finds receiver by [[id]]
     *
     * @return User|null
     */
    public function getReceiver()
    {
        if(!$this->_receiver)
        {
            $this->_receiver = $this->getUser($this->receiver_id);
        }
        return $this->_receiver;
    }
    

    /**
     * Finds user by [[id]]
     *
     * @return User|null
     */
    public function getUser($id)
    {
        $user = User::findIdentity($id);   
        return $user;
    }
    
    public function validateSum()
    {

        $sum = $this->sum;
        $sender = $this->getSender();
        $limit = Yii::$app->params['limit'] ? (float)Yii::$app->params['limit'] : -1000;
        if (!$sender) {
            $this->addError('sum', 'Sender record is not found');
            return false;
        }
        if($sender->balance - $sum < $limit)
        {
            $available = $sender->balance - $limit;
            $this->addError('sum', 'You cannot send more than ' . number_format($available, 2) . ' units');
            return false;
        }
        return true;
    }
}
