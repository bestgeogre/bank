<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username is required
            [['username'], 'required'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            Yii::trace("Username: " . $this->username);
            $user = $this->getUser();
            Yii::trace("Current user: " . print_r($user, true));
            return Yii::$app->user->login($user, 3600*24*30);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
            if($this->_user === null)
            {
                $user = new User(['username' => $this->username]);
                $user->generateAuthKey();
                if($user->save())
                {
                    $this->_user = $user;
                }
                else
                {
                    Yii::trace("Get user errors: " . print_r($user->getErrors(), true));
                }
            }
        }

        return $this->_user;
    }
}
