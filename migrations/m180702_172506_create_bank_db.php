<?php

use yii\db\Migration;

/**
 * Class m180702_172506_create_bank_db
 */
class m180702_172506_create_bank_db extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('transactions', $tables))  { 
        if ($dbType == "mysql") {
                $this->createTable('{{%transactions}}', [
                        'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                        0 => 'PRIMARY KEY (`id`)',
                        'sender_id' => 'INT(11) NOT NULL',
                        'recipient_id' => 'INT(11) NOT NULL',
                        'sum' => 'FLOAT NOT NULL',
                        'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ',
                ], $tableOptions_mysql);
        }
        }

        /* MYSQL */
        if (!in_array('user', $tables))  { 
        if ($dbType == "mysql") {
                $this->createTable('{{%user}}', [
                        'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                        0 => 'PRIMARY KEY (`id`)',
                        'username' => 'VARCHAR(255) NOT NULL',
                        'auth_key' => 'VARCHAR(255) NOT NULL',
                        'accessToken' => 'VARCHAR(255) NULL',
                        'balance' => 'FLOAT NOT NULL',
                        'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ',
                ], $tableOptions_mysql);
        }
        }


        $this->createIndex('idx_sender_id_8496_00','transactions','sender_id',0);
        $this->createIndex('idx_recipient_id_8496_01','transactions','recipient_id',0);
        $this->createIndex('idx_UNIQUE_username_852_02','user','username',1);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_user_849_00','{{%transactions}}', 'recipient_id', '{{%user}}', 'id', 'NO ACTION', 'CASCADE' );
        $this->addForeignKey('fk_user_849_01','{{%transactions}}', 'sender_id', '{{%user}}', 'id', 'NO ACTION', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `transactions`');
        $this->execute('SET foreign_key_checks = 1;');
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `user`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180702_172506_create_bank_db cannot be reverted.\n";

        return false;
    }
    */
}
